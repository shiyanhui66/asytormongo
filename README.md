Dependencies
-----------

* tornado
* mongodb
* motor

Install
--------

    python setup.py install 

Example
-------

    import motor
    from tornado import gen
	from tornado.web import asynchronous
	from asytormongo.field import *
	from asytormongo.model import Model

	class ExampleModel( Model ):
		name = StringField( required=True, max_length=10 )
		email = EmailField( required=True )
		password = IntField( max=10000000 )

		meta = {
			#default is test
			'db':'example_db',

			#default is the class name
			'collection':'example_collection'
		}

		@staticmethod
		@gen.engine
		def your_method( arg, callback ):
			result = yield motor.Op( .... )
			callback( result )

	class ExampleHandler( BaseHandler ):
		@asynchronous
		@gen.engine
		def get( self ):
			result = yield gen.Task( ExampleModel().your_method, arg )
			self.finish()
		
Notice
------

When you write you custom method, you shoud notice:

* must have a parameter named callback
* cann't use * and ** parameter

see the example.

About
-----

Asytormongo is not a pure ORM. it packages the fields in models and examines the field when 
deal with database. it makes clear that what fields there in your models.
