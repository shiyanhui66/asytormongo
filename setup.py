from distutils.core import setup

setup(
	name = 'asytormongo',
	version = '1.0.0',
	author = 'Shi Yanhui', 
	author_email = 'shiyanhui66@gmail.com',
	url = 'https://github.com/shiyanhui/asytormongo',
	description = 'Asynchronous ORM for tornado with mongodb',
	license = 'Apache License, Version 2.0',
	packages = [ 'asytormongo' ],
	classifiers = [
		'Programming Language :: Python',
		'Programming Language :: Python :: 3',
		'Development Status :: 1 - Beta',
		'Environment :: Other Environment',
		'Intended Audience :: Developers',
		'License :: OSI Approved :: GNU Library or Lesser General Public License (LGPL)',
		'Operating System :: OS Independent',
		'Topic :: Software Development :: Libraries :: Python Modules',
	],
)