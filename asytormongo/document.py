#coding=utf-8

'''
用法例子：
class ExampleDocument( Document ):
	name = StringField( required=True, max_length=10 )
	email = EmailField( required=True )
	password = IntField( max=10000000 )

	meta = {
		'collection':'example'
	}

	@gen.engine
	def your_method( arg, callback ):
		result = yield motor.Op( .... )
		callback( result, None )

class ExampleHandler( BaseHandler ):
	@asynchronous
	@gen.engine
	def get( self ):
		result = yield motor.Op( ExampleDocument().your_method, arg )
		......

'''
import motor
from tornado import gen
from asytormongo.field import *
from exceptions import Exception
from bson.dbref import DBRef
from bson.objectid import ObjectId

db = motor.MotorClient().open_sync().Qingcong

class Document( object ):
	def __init__( self ):
		#该document所具有的类属性
		self.class_attr = self.__class__.__dict__
		#该集合的名字
		self.collection_name = self.meta[ 'collection' ] if 'meta' in self.class_attr and 'collection' in self.meta else self.__class__.__name__
		#该集合
		self.collection = eval( 'db.' + self.collection_name.lower() )
		#该集合中的field
		self.field_list = [ attr for attr in self.class_attr if isinstance( self.class_attr[ attr ], BaseField ) ]
 
	@gen.engine
	def insert( self, document, callback=None ):
		#先检验传进来的document是否符合规则
		for field_name in document:
			if field_name in self.field_list:
				#进行验证
				setattr( self.class_attr[ field_name ], 'value', document[ field_name ]  )
				getattr( self.class_attr[ field_name ], 'validate', None )()
			else:
				raise Exception, 'cann\'t find %s in %s' % ( field_name, self.__class__.__name__ )

			#进行unique验证
			if getattr( self.class_attr[ field_name ], 'unique', False ):
				result = yield motor.Op( self.collection.find_one, { field_name: document[ field_name ] } )
				if result:
					raise Exception, '%s is unique!' % field_name

		#进行required验证
		for field_name in self.field_list:
			if getattr( self.class_attr[ field_name ], 'required', False ) and field_name not in document:
				raise Exception, '%s is required!' % field_name

		result = yield motor.Op( self.collection.insert, document )
		callback( result, None )

	@gen.engine
	def find_one( self, condition, callback=None ):
		result = yield motor.Op( self.collection.find_one, condition )
		callback( result, None )

	@gen.engine
	def update( self, condition, data, callback=None ):
		result = yield motor.Op( self.collection.update, condition, data )
		callback( result, None )
		
	@gen.engine
	def remove( self, condition, callback=None ):
		result = yield motor.Op( self.collection.remove, condition )
		callback( result, None )
		
	#根据DBRef获取对应的document
	@staticmethod
	@gen.engine
	def translate_one_dbref( dbref, callback=None ):
		collection = eval( 'db.' + dbref.collection )
		result = yield motor.Op( collection.find_one, { '_id': ObjectId( dbref.id ) } )
		callback( result, None )

	#把document中的所有的DBRef对应的document都获取过来
	@staticmethod
	@gen.engine
	def translate_dbref_in_one_document( document, callback=None ):
		for field in document:
			if isinstance( document[ field ], DBRef ):
				document[ field ] = yield motor.Op( Document.translate_one_dbref, document[ field ] )
		callback( document, None )

	#将document_list中的dbref域所对应的document获取过来
	@staticmethod
	@gen.engine
	def translate_dbref_in_document_list( document_list, callback=None ):
		for document in document_list:
			document = yield motor.Op( Document.translate_dbref_in_one_document, document )
		callback( document_list, None )
