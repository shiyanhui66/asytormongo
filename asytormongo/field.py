#coding=utf-8
import re
from exceptions import Exception
from datetime import datetime, date
from bson.dbref import DBRef
from bson.binary import Binary

__all__ = [ 'BaseField','StringField', 'IntegerField', 'FloatField', 'BoolField', 'DateField', 'DateTimeField', 'EmailField', 'ReferenceField',\
			'EmbeddedDocumentField', 'ListField', 'BinaryField' ]

class BaseField( object ):
	def __init__( self, required=False, default=None, unique=False, candidate=None ):
		self.required = required
		self.default = default
		self.unique = unique
		self.candidate = candidate
		self.value = None

	#检验该值是否在candidate中
	def validate( self ):
		if self.candidate and self.value not in self.candidate:
			raise Exception, '%s not in %s' % ( self.value, self.candidate )

class StringField( BaseField ):
	def __init__( self, required=False, default=None, unique=False, candidate=None, max_length=None, min_length=None, regex=None ):
		super( StringField, self ).__init__( required, default, unique, candidate )
		#validate
		if max_length is not None and not isinstance( max_length, ( int, long ) ):
			raise Exception, 'max_length should be integer value'
		
		if min_length is not None and not isinstance( min_length, ( int, long ) ):
			raise Exception, 'min_length should be integer value'
		
		if regex is not None and not isinstance( regex, str ):
			raise Exception, 'regex should be string value'

		#assign
		self.max_length, self.min_length = max_length, min_length
		self.regex = regex

	def validate( self ):
		super( StringField, self ).validate()
		#validate whether it is string type
		if self.value is not None:
			if self.max_length is not None and len( self.value ) > self.max_length: 
				raise Exception, 'string value is too long'

			if self.min_length is not None and len( self.value ) < self.min_length:
				raise Exception, 'string value is too short'

			if self.regex is not None and re.match( regex, self.value ) is None:
				raise Exception, 'regex doesn\'t match'

class IntegerField( BaseField ):
	def __init__( self, required=False, default=None, unique=False, candidate=None, max=None, min=None ):
		super( IntegerField, self ).__init__( required, default, unique, candidate )
		if max is not None and not isinstance( max, ( int, long ) ):
			raise Exception, 'max should be int value'

		if min is not None and not isinstance( min, ( int, long ) ):
			raise Exception, 'min should be in value'

		self.max, self.min = max, min 

	def validate( self ):
		super( IntegerField, self ).validate()
		if self.value is not None:
			try:
				self.value = int( self.value )
			except:
				raise Exception, '%s cann\'t be converted to int value' % self.value

			if self.max is not None and self.value > self.max:
				raise Exception, '%s is larger than max %s' % ( self.value, self.max )

			if self.min is not None and self.value < self.min:
				raise Exception, '%s is less than min %s' % ( self.value, self.min )

class FloatField( BaseField ):
	def __init__( self, required=False, default=None, unique=False, candidate=None, max=None, min=None ):
		super( FloatField, self ).__init__( required, default, unique, candidate )
		if max is not None:
			try:
				max = float( max )
			except:
				raise Exception, 'max should be float value'

		if max is not None:
			try:
				min = float( min )
			except:
				raise Exception, 'min should be float value'

		self.max, self.min = max, min

	def validate( self ):
		super( FloatField, self ).validate()
		if self.value is not None:
			try:
				self.value = float( self.value )
			except:
				raise Exception, '%s cann\'t be converted to float value' % self.value

			if self.max is not None and self.value > self.max:
				raise Exception, '%s is larger than max %s' % ( self.value, self.max )

			if self.min is not None and self.value < self.min:
				raise Exception, '%s is less than min %s' % ( self.value, self.min )

class BoolField( BaseField ):
	def validate( self ):
		super( BoolField, self ).validate()
		if self.value is not None:
			if not isinstance( self.value, bool ):
				raise Exception, '%s is not a bool value' % self.value

class DateField( BaseField ):
	def validate( self ):
		super( DateField, self ).validate()
		if self.value is not None:
			if not isinstance( self.value, date ):
				raise Exception, '%s is not a date value' % self.value

class DateTimeField( BaseField ):
	def validate( self ):
		super( DateTimeField, self ).validate()
		if self.value is not None:
			if not isinstance( self.value, datetime ):
				raise Exception, '%s is not a datetime value' % self.value

class IDField( BaseField ):
	def validate( self ):
		super( IDField, self ).validate()
		if self.value is not None:
			if not isinstance( self.value, str ):
				raise Exception, 'IDField should be string value'
			if len( self.value ) != 15 and len( self.value ) != 18:
				raise Exception, 'IDField\'s length should be 15 or 18'
			if len( self.value ) == 15:
				if self.value.isdigit():
					raise Exception, 'IDField of 15 length should be digit'

			elif len( self.value ) == 18:
				pass

class EmailField( StringField ):
	def validate( self ):
		super( EmailField, self ).validate()
		if self.value is not None:
			regex = re.compile(
				    r"(^[-!#$%&'*+/=?^_`{}|~0-9A-Z]+(\.[-!#$%&'*+/=?^_`{}|~0-9A-Z]+)*"  # dot-atom
				    # quoted-string, see also http://tools.ietf.org/html/rfc2822#section-3.2.5
				    r'|^"([\001-\010\013\014\016-\037!#-\[\]-\177]|\\[\001-\011\013\014\016-\177])*"'
				    r')@((?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+[A-Z]{2,6}\.?$)'  # domain
				    r'|\[(25[0-5]|2[0-4]\d|[0-1]?\d?\d)(\.(25[0-5]|2[0-4]\d|[0-1]?\d?\d)){3}\]$', re.IGNORECASE )
			if not re.match( regex , self.value ):
				raise Exception, '%s is not email format' % self.value

class ReferenceField( BaseField ):
	def __init__( self, referenced_document=None, required=False, default=None, unique=False, candidate=None ):
		super( ReferenceField, self ).__init__( required, default, unique, candidate )
		# referenced_document只是为了标明是哪个document的，查询的时候用不到
		self.referenced_document = referenced_document

	def validate( self ):
		super( ReferenceField, self ).validate()
		if self.value is not None and not isinstance( self.value, DBRef ):
			raise Exception, '%s is not instance of DBRef' % self.value
	
class EmbeddedDocumentField( BaseField ):
	def __init__( self, embedded_document, required=False, default=None, unique=False, candidate=None ):
		super( EmbeddedDocumentField, self ).__init__( required, default, unique, candidate )
		self.embedded_document = embedded_document

	def validate( self ):
		super( EmbeddedDocumentField, self ).validate()
		if self.value is not None and not isinstance( self.value, dict ):
			raise Exception, '%s is not instance of dict' % self.value

class ListField( BaseField ):
	def validate( self ):
		super( ListField, self ).validate()
		if self.value is not None and not isinstance( self.value, list ):
			raise Exception, '%s is not instance of list' % self.value

# 二进制存储域，可以存储图片、视频、程序等二进制数据
class BinaryField( BaseField ):
	def validate( self ):
		super( BinaryField, self ).validate()
		if self.value is not None and not isinstance( self.value, Binary ):
			raise Exception, '%s should be a instance of Binary' % self.__class__.__name__
